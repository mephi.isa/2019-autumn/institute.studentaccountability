import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import SignIn from '@/components/SignIn'
import Groups from '@/components/Groups'
import Group from '@/components/Group'
import Student from '@/components/Student'
import OrderLists from '@/components/OrderLists'
import OrderList from '@/components/OrderList'
import Criterion from '@/components/Criterion'
import UserPage from '@/components/UserPage'
import NewOrderList from '@/components/NewOrderList'
import NewCriterion from '@/components/NewCriterion'
import AdminPage from '@/components/AdminPage'
import SignInAdmin from '@/components/SignInAdmin'
import EditUser from '@/components/EditUser'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/home',
      name: 'Home',
      component: Home
    },
    {
      path: '/login',
      name: 'SignIn',
      component: SignIn
    },
    {
      path: '/groups',
      name: 'Groups',
      component: Groups
    },
    {
      path: '/group/:groupName',
      name: 'Group',
      component: Group
    },
    {
      path: '/student/:groupName/:studentName',
      name: 'Student',
      component: Student
    },
    {
      path: '/orders',
      name: 'Orders',
      component: OrderLists
    },
    {
      path: '/criterion/:criterionName',
      name: 'Criterion',
      component: Criterion
    },
    {
      path: '/order/:orderName',
      name: 'Order',
      component: OrderList
    },
    {
      path: '/new_order',
      name: 'NewOrder',
      component: NewOrderList
    },
    {
      path: '/new_criterion',
      name: 'NewCriterion',
      component: NewCriterion
    },
    {
      path: '/user',
      name: 'UserPage',
      component: UserPage
    },
    {
      path: '/admin/login',
      name: 'SignInAdmin',
      component: SignInAdmin
    },
    {
      path: '/admin/edit/:userName',
      name: 'EditUser',
      component: EditUser
    },
    {
      path: '/admin/new',
      name: 'NewUser',
      component: EditUser
    },
    {
      path: '/admin',
      name: 'AdminPage',
      component: AdminPage
    }
  ]
})