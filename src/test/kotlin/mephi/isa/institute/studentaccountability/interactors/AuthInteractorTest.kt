package mephi.isa.institute.studentaccountability.interactors

import mephi.isa.institute.studentaccountability.model.Role
import mephi.isa.institute.studentaccountability.model.User
import mephi.isa.institute.studentaccountability.repository.InMemoryUserRepository
import mephi.isa.institute.studentaccountability.storage.AuthStorage
import mephi.isa.institute.studentaccountability.util.UnauthorizedException
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class AuthInteractorTest {
    private val authRepository = InMemoryUserRepository(
        mutableMapOf(
            "tutor" to Pair(User("tutor", Role.DepartmentManager, "IT"), "password")
        ),
        mutableMapOf("admin" to "qwerty")
    )

    @Test
    fun userAuthTest() {
        val authStorage = AuthStorage(10)
        val interactor = AuthInteractor(authStorage, authRepository)
        val token = interactor.authenticate("tutor", "password")
        assertTrue(authStorage.hasUser(token))
        Thread.sleep(11)
        assertFalse(authStorage.hasUser(token))

        val newToken = interactor.authenticate("tutor", "password")
        assertTrue(authStorage.hasUser(newToken))
        assertEquals(authStorage.getUser(newToken), User("tutor", Role.DepartmentManager, "IT"))
        interactor.deauthenticate(newToken)
        assertFalse(authStorage.hasUser(newToken))
    }

    @Test(expected = UnauthorizedException::class)
    fun userIncorrectPasswordAuthTest() {
        val authStorage = AuthStorage(10)
        val interactor = AuthInteractor(authStorage, authRepository)
        interactor.authenticate("tutor", "123")
    }

    @Test(expected = UnauthorizedException::class)
    fun userIncorrectLoginAuthTest() {
        val authStorage = AuthStorage(10)
        val interactor = AuthInteractor(authStorage, authRepository)
        interactor.authenticate("admin", "qwerty")
    }

    @Test(expected = UnauthorizedException::class)
    fun incorrectDeauthTest() {
        val interactor = AuthInteractor(AuthStorage(10), authRepository)
        interactor.deauthenticate("invalid token")
    }

    @Test(expected = UnauthorizedException::class)
    fun incorrectAuthTest() {
        AuthStorage(10).getUser("invalid token")
    }
}