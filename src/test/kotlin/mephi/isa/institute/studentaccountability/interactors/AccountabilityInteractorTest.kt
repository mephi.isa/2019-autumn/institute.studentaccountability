package mephi.isa.institute.studentaccountability.interactors

import mephi.isa.institute.studentaccountability.util.NotFoundException
import mephi.isa.institute.studentaccountability.model.*
import mephi.isa.institute.studentaccountability.repository.InMemoryStudentRepository
import mephi.isa.institute.studentaccountability.repository.InMemoryUserRepository
import mephi.isa.institute.studentaccountability.storage.AuthStorage
import mephi.isa.institute.studentaccountability.util.UnauthorizedException
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class AccountabilityInteractorTest {
    private val authRepository = InMemoryUserRepository(
        mutableMapOf(
            "tutor_i" to Pair(User("tutor_i", Role.DepartmentManager, "IT"), "password"),
            "tutor_p" to Pair(User("tutor_p", Role.DepartmentManager, "Physics"), "password"),
            "dean" to Pair(User("dean", Role.InstituteManager), "password")
        ),
        mutableMapOf("admin" to "qwerty")
    )

    private val authStorage = AuthStorage(60 * 60 * 1000)
    private val authInteractor = AuthInteractor(authStorage, authRepository)
    private val token: String = authInteractor.authenticate("dean", "password")
    private val tokenI: String = authInteractor.authenticate("tutor_i", "password")
    private val tokenP: String = authInteractor.authenticate("tutor_p", "password")

    enum class SubjectV(val v: Subject) {
        MATHS(Subject("Maths", 2019, 12, true)),
        PHYSICS(Subject("Physics", 2019, 12, true)),
        PROG_I(Subject("Programming", 2019, 12, true)),
        PROG_P(Subject("Programming", 2019, 12, false)),
        ENGLISH(Subject("English", 2019, 12, false)),
        DOCS(Subject("Tech documentation", 2019, 12, false))
    }

    private val noAttemptsCriterion = Criterion(
        "Not attempted",
        0,
        CriterionApplication.None,
        MatchMode.Equal,
        1,
        CriterionApplication.All,
        MatchMode.Below
    )

    private val studentRepository = InMemoryStudentRepository(
        mutableSetOf(
            SubjectV.MATHS.v,
            SubjectV.PHYSICS.v,
            SubjectV.DOCS.v,
            SubjectV.ENGLISH.v,
            SubjectV.PROG_I.v,
            SubjectV.PROG_P.v
        ),
        mutableMapOf(
            "I1-1" to Group(
                "I1-1", 1, "IT", setOf(
                    Student(
                        "John", "Doe", mapOf(
                            SubjectV.MATHS.v to Progress(SubjectV.MATHS.v, 90, 1),
                            SubjectV.PROG_I.v to Progress(SubjectV.PROG_I.v, 69, 2),
                            SubjectV.ENGLISH.v to Progress(SubjectV.ENGLISH.v, 100, 1),
                            SubjectV.DOCS.v to Progress(SubjectV.DOCS.v, 84, 1)
                        )
                    ),
                    Student(
                        "Jane", "Smith", mapOf(
                            SubjectV.MATHS.v to Progress(SubjectV.MATHS.v, 70, 1),
                            SubjectV.PROG_I.v to Progress(SubjectV.PROG_I.v, 60, 1),
                            SubjectV.ENGLISH.v to Progress(SubjectV.ENGLISH.v, 90, 1),
                            SubjectV.DOCS.v to Progress(SubjectV.DOCS.v, 55, 5)
                        )
                    )
                )
            ),
            "P1-1" to Group(
                "P1-1", 1, "Physics", setOf(
                    Student(
                        "John", "Smith", mapOf(
                            SubjectV.MATHS.v to Progress(SubjectV.MATHS.v, 95, 1),
                            SubjectV.PHYSICS.v to Progress(SubjectV.PHYSICS.v, 99, 1),
                            SubjectV.ENGLISH.v to Progress(SubjectV.ENGLISH.v, 100, 1),
                            SubjectV.PROG_P.v to Progress(SubjectV.PROG_P.v, 92, 1)
                        )
                    ),
                    Student(
                        "Jane", "Doe", mapOf(
                            SubjectV.MATHS.v to Progress(SubjectV.MATHS.v, 70, 2),
                            SubjectV.PHYSICS.v to Progress(SubjectV.PHYSICS.v, 60, 3),
                            SubjectV.ENGLISH.v to Progress(SubjectV.ENGLISH.v, 75, 3),
                            SubjectV.PROG_P.v to Progress(SubjectV.PROG_P.v, 65, 3)
                        )
                    )
                )
            )
        ),
        mutableMapOf(
            "No attempts 0" to OrderList(
                "No attempts 0", noAttemptsCriterion, listOf(
                    Pair(
                        Student("Ivan", "Petroff", mapOf(SubjectV.ENGLISH.v to Progress(SubjectV.ENGLISH.v, 0, 0))),
                        Group("RI0-1", 1, "IT", setOf())
                    ),
                    Pair(
                        Student("Mikhail", "Sidoroff", mapOf(SubjectV.ENGLISH.v to Progress(SubjectV.ENGLISH.v, 0, 0))),
                        Group("RP0-1", 1, "Physics", setOf())
                    )
                )
            ),
            "No attempts 1" to OrderList(
                "No attempts 1", noAttemptsCriterion, listOf(
                    Pair(
                        Student("Ivan", "Petroff", mapOf(SubjectV.ENGLISH.v to Progress(SubjectV.ENGLISH.v, 0, 0))),
                        Group("RI0-1", 1, "IT", setOf())
                    )
                )
            ),
            "No attempts 2" to OrderList(
                "No attempts 1", noAttemptsCriterion, listOf(
                    Pair(
                        Student("Mikhail", "Sidoroff", mapOf(SubjectV.ENGLISH.v to Progress(SubjectV.ENGLISH.v, 0, 0))),
                        Group("RP0-1", 1, "Physics", setOf())
                    )
                )
            )
        ),
        mutableMapOf(noAttemptsCriterion.name to noAttemptsCriterion),
        mutableSetOf("Physics", "IT")
    )

    private val accountabilityInteractor = AccountabilityInteractor(authStorage, studentRepository)


    @Test
    fun deptsListTest() {
        assertEquals(setOf("IT", "Physics"), accountabilityInteractor.listDepts(token).toSet())
        assertEquals(setOf("IT", "Physics"), accountabilityInteractor.listDepts(tokenI).toSet())
        assertEquals(setOf("IT", "Physics"), accountabilityInteractor.listDepts(tokenP).toSet())
    }

    @Test(expected = UnauthorizedException::class)
    fun unauthorizedDeptsListTest() {
        accountabilityInteractor.listDepts("none")
    }

    @Test
    fun groupsListTest() {
        assertEquals(setOf("I1-1", "P1-1"), accountabilityInteractor.listGroups(token))
        assertEquals(setOf("I1-1"), accountabilityInteractor.listGroups(tokenI))
        assertEquals(setOf("P1-1"), accountabilityInteractor.listGroups(tokenP))
    }

    @Test(expected = UnauthorizedException::class)
    fun unauthorizedGroupsListTest() {
        accountabilityInteractor.listGroups("none")
    }

    @Test
    fun allStudentsListTest() {
        assertEquals(
            setOf(
                Triple("John", "Doe", "I1-1"),
                Triple("Jane", "Smith", "I1-1"),
                Triple("John", "Smith", "P1-1"),
                Triple("Jane", "Doe", "P1-1")
            ), accountabilityInteractor.listAllStudents(token).toSet()
        )
        assertEquals(
            setOf(Triple("John", "Doe", "I1-1"), Triple("Jane", "Smith", "I1-1")),
            accountabilityInteractor.listAllStudents(tokenI).toSet()
        )
        assertEquals(
            setOf(Triple("John", "Smith", "P1-1"), Triple("Jane", "Doe", "P1-1")),
            accountabilityInteractor.listAllStudents(tokenP).toSet()
        )
    }

    @Test(expected = UnauthorizedException::class)
    fun unauthorizedAllStudentsListTest() {
        accountabilityInteractor.listAllStudents("none")
    }

    @Test
    fun groupStudentsListTest() {
        assertEquals(
            setOf(Triple("John", "Doe", "I1-1"), Triple("Jane", "Smith", "I1-1")),
            accountabilityInteractor.listGroupStudents(token, "I1-1").toSet()
        )
        assertEquals(
            setOf(Triple("John", "Doe", "I1-1"), Triple("Jane", "Smith", "I1-1")),
            accountabilityInteractor.listGroupStudents(tokenI, "I1-1").toSet()
        )
    }

    @Test(expected = UnauthorizedException::class)
    fun disallowedGroupStudentsListTest() {
        accountabilityInteractor.listGroupStudents(tokenP, "I1-1")
    }

    @Test(expected = UnauthorizedException::class)
    fun unauthorizedGroupStudentsListTest() {
        accountabilityInteractor.listGroupStudents("none", "I1-1")
    }

    @Test
    fun criteriaListTest() {
        assertEquals(setOf(noAttemptsCriterion.name), accountabilityInteractor.listCriteria(tokenP))
    }

    @Test(expected = UnauthorizedException::class)
    fun unauthorizedCriteriaListTest() {
        accountabilityInteractor.listCriteria("none")
    }

    @Test
    fun orderListTest() {
        assertEquals(setOf("No attempts 0", "No attempts 1", "No attempts 2"), accountabilityInteractor.listOrderLists(token))
        assertEquals(setOf("No attempts 0", "No attempts 1", "No attempts 2"), accountabilityInteractor.listOrderLists(tokenI))
        assertEquals(setOf("No attempts 0", "No attempts 1", "No attempts 2"), accountabilityInteractor.listOrderLists(tokenP))
    }

    @Test(expected = UnauthorizedException::class)
    fun unauthorizedListOrderTest() {
        accountabilityInteractor.listOrderLists("none")
    }

    @Test(expected = UnauthorizedException::class)
    fun unauthorizedOrderListTest() {
        accountabilityInteractor.listCriteria("none")
    }

    @Test
    fun getOrderListTest() {
        assertEquals(OrderList(
            "No attempts 0", noAttemptsCriterion, listOf(
                Pair(
                    Student("Ivan", "Petroff", mapOf(SubjectV.ENGLISH.v to Progress(SubjectV.ENGLISH.v, 0, 0))),
                    Group("RI0-1", 1, "IT", setOf())
                ),
                Pair(
                    Student("Mikhail", "Sidoroff", mapOf(SubjectV.ENGLISH.v to Progress(SubjectV.ENGLISH.v, 0, 0))),
                    Group("RP0-1", 1, "Physics", setOf())
                )
            )
        ), accountabilityInteractor.getOrderList(token, "No attempts 0"))
    }

    @Test(expected = UnauthorizedException::class)
    fun disallowedGetOrderListTest() {
        accountabilityInteractor.getOrderList(tokenI, "No attempts 0")
    }

    @Test(expected = UnauthorizedException::class)
    fun unauthorizedGetOrderListTest() {
        accountabilityInteractor.getOrderList("none", "No attempts 0")
    }

    @Test
    fun getCriterionTest() {
        assertEquals(noAttemptsCriterion, accountabilityInteractor.getCriterion(token, noAttemptsCriterion.name))
    }

    @Test(expected = UnauthorizedException::class)
    fun unauthorizedGetCriterionTest() {
        accountabilityInteractor.getCriterion("none", "No attempts 0")
    }

    private fun addGoodMarkCriterion() {
        val newCriterion = Criterion("GoodMarkCriterion", 89, CriterionApplication.All, MatchMode.Above, 0, CriterionApplication.None, MatchMode.Equal, true)
        accountabilityInteractor.addCriterion(token, newCriterion)
    }

    private fun addGoldMarkCriterion() {
        val newCriterion = Criterion("GoldMarkCriterion", 100, CriterionApplication.Once, MatchMode.Equal, 0, CriterionApplication.None, MatchMode.Equal, false)
        accountabilityInteractor.addCriterion(token, newCriterion)
    }

    private fun addTooMuchAttemptsCriterion() {
        val newCriterion = Criterion("TooMuchAttempts", 0, CriterionApplication.None, MatchMode.Equal,2, CriterionApplication.Average, MatchMode.Above)
        accountabilityInteractor.addCriterion(token, newCriterion)
    }

    @Test
    fun addCriteriaTest() {
        addGoodMarkCriterion()
        assertTrue("GoodMarkCriterion" in accountabilityInteractor.listCriteria(token))
        assertFalse("TooMuchAttempts" in accountabilityInteractor.listCriteria(token))

        addTooMuchAttemptsCriterion()
        assertTrue("TooMuchAttempts" in accountabilityInteractor.listCriteria(token))
    }

    @Test(expected = UnauthorizedException::class)
    fun unauthorizedAddCriterionTest() {
        accountabilityInteractor.addCriterion("none", noAttemptsCriterion)
    }
    
    @Test
    fun createOrderTest() {
        addTooMuchAttemptsCriterion()
        addGoodMarkCriterion()

        accountabilityInteractor.createOrder(token, "TooMuchAttempts 0", "TooMuchAttempts", setOf("P1-1","I1-1"))
        var list = accountabilityInteractor.getOrderList(token, "TooMuchAttempts 0")
        assertEquals(listOf("Jane"), list.students.map{pair -> pair.first.name})

        accountabilityInteractor.createOrder(token, "GoodMarkCriterion 0", "GoodMarkCriterion", setOf("P1-1","I1-1"))
        list = accountabilityInteractor.getOrderList(token, "GoodMarkCriterion 0")
        assertEquals(listOf("John"), list.students.map{pair -> pair.first.name})

        addGoldMarkCriterion()

        accountabilityInteractor.createOrder(token, "GoldMarkCriterion 0", "GoldMarkCriterion", setOf("P1-1","I1-1"))
        list = accountabilityInteractor.getOrderList(token, "GoldMarkCriterion 0")
        assertEquals(listOf("John", "John"), list.students.map{pair -> pair.first.name}.toList())
    }

    @Test(expected = NotFoundException::class)
    fun nonexistentCriterionCreateOrderTest() {
        accountabilityInteractor.createOrder(token, "New", "TooMuchAttempts1", setOf("P1-1"))
    }

    @Test(expected = NotFoundException::class)
    fun nonexistentGroupCreateOrderTest() {
        accountabilityInteractor.createOrder(token, "New", noAttemptsCriterion.name, setOf("P1-2"))
    }

    @Test(expected = UnauthorizedException::class)
    fun disallowedCreateOrderTest() {
        accountabilityInteractor.createOrder(tokenI, "New", noAttemptsCriterion.name, setOf("P1-1"))
    }

    @Test(expected = UnauthorizedException::class)
    fun unauthorizedCreateOrderTest() {
        accountabilityInteractor.createOrder("none", "New", "New", setOf())
    }

}