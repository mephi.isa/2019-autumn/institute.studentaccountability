package mephi.isa.institute.studentaccountability.interactors

import junit.framework.TestCase.assertFalse
import junit.framework.TestCase.assertTrue
import mephi.isa.institute.studentaccountability.model.*
import mephi.isa.institute.studentaccountability.repository.InMemoryStudentRepository
import mephi.isa.institute.studentaccountability.storage.AuthStorage
import org.junit.Test


class CriterionMatcherTest {

    private val authStorage = AuthStorage(0)
    private val studentRepository =
        InMemoryStudentRepository(mutableSetOf(), mutableMapOf(), mutableMapOf(), mutableMapOf(), mutableSetOf())
    private val interactor = AccountabilityInteractor(authStorage, studentRepository)

    @Test
    fun markTest() {
        val math = Subject("Math", 2019, 11, true)
        val history = Subject("History", 2019, 11, false)
        val mathProgress = Progress(math, 89, 2)
        val historyProgress = Progress(history, 99, 1)
        val student = Student("John", "Doe", mapOf(math to mathProgress, history to historyProgress))

        val criterionMark80 = Criterion(
            "80+",
            80,
            CriterionApplication.All,
            MatchMode.Above,
            0,
            CriterionApplication.None,
            MatchMode.Equal,
            false
        )
        val criterionMark80below = Criterion(
            "80+",
            80,
            CriterionApplication.All,
            MatchMode.Below,
            0,
            CriterionApplication.None,
            MatchMode.Equal,
            false
        )
        val criterionMark80ex = Criterion(
            "80+",
            80,
            CriterionApplication.All,
            MatchMode.Above,
            0,
            CriterionApplication.None,
            MatchMode.Equal,
            true
        )
        val criterionMark80exBelow = Criterion(
            "80+",
            80,
            CriterionApplication.All,
            MatchMode.Below,
            0,
            CriterionApplication.None,
            MatchMode.Equal,
            true
        )

        val criterionMark90ex = Criterion(
            "90+",
            90,
            CriterionApplication.All,
            MatchMode.Above,
            0,
            CriterionApplication.None,
            MatchMode.Equal,
            true
        )
        val criterionMark90exBelow = Criterion(
            "90+",
            90,
            CriterionApplication.All,
            MatchMode.Below,
            0,
            CriterionApplication.None,
            MatchMode.Equal,
            true
        )

        val criterionMark89exEq = Criterion(
            "89=",
            89,
            CriterionApplication.All,
            MatchMode.Equal,
            0,
            CriterionApplication.None,
            MatchMode.Equal,
            true
        )

        val criterionMark88exEq = Criterion(
            "88=",
            88,
            CriterionApplication.All,
            MatchMode.Equal,
            0,
            CriterionApplication.None,
            MatchMode.Equal,
            true
        )

        val criterionMark88onceEq = Criterion(
            "88=",
            88,
            CriterionApplication.Once,
            MatchMode.Equal,
            0,
            CriterionApplication.None,
            MatchMode.Equal
        )

        val criterionMark99onceEq = Criterion(
            "99=",
            99,
            CriterionApplication.Once,
            MatchMode.Equal,
            0,
            CriterionApplication.None,
            MatchMode.Equal
        )

        val criterionMark94avgEq = Criterion(
            "94a=",
            94,
            CriterionApplication.Average,
            MatchMode.Equal,
            0,
            CriterionApplication.None,
            MatchMode.Equal
        )

        val criterionMark90avgBelow = Criterion(
            "90a-",
            90,
            CriterionApplication.Average,
            MatchMode.Below,
            0,
            CriterionApplication.None,
            MatchMode.Equal
        )

        val criterionMark90avgAbove = Criterion(
            "90a+",
            90,
            CriterionApplication.Average,
            MatchMode.Above,
            0,
            CriterionApplication.None,
            MatchMode.Equal
        )

        val criterionMark188Total = Criterion(
            "188t=",
            188,
            CriterionApplication.Total,
            MatchMode.Equal,
            0,
            CriterionApplication.None,
            MatchMode.Equal
        )

        assertTrue(interactor.matchStudent(student, criterionMark80))
        assertFalse(interactor.matchStudent(student, criterionMark80below))
        assertTrue(interactor.matchStudent(student, criterionMark80ex))
        assertFalse(interactor.matchStudent(student, criterionMark80exBelow))
        assertFalse(interactor.matchStudent(student, criterionMark90ex))
        assertTrue(interactor.matchStudent(student, criterionMark90exBelow))
        assertTrue(interactor.matchStudent(student, criterionMark89exEq))
        assertFalse(interactor.matchStudent(student, criterionMark88exEq))
        assertTrue(interactor.matchStudent(student, criterionMark99onceEq))
        assertFalse(interactor.matchStudent(student, criterionMark88onceEq))
        assertTrue(interactor.matchStudent(student, criterionMark94avgEq))
        assertTrue(interactor.matchStudent(student, criterionMark90avgAbove))
        assertFalse(interactor.matchStudent(student, criterionMark90avgBelow))
        assertTrue(interactor.matchStudent(student, criterionMark188Total))

    }

    @Test
    fun attemptsTest() {
        val math = Subject("Math", 2019, 11, true)
        val history = Subject("History", 2019, 11, false)
        val mathProgress = Progress(math, 89, 2)
        val historyProgress = Progress(history, 99, 1)
        val student = Student("John", "Doe", mapOf(math to mathProgress, history to historyProgress))

        val criterionAttempts1 = Criterion(
            "a0+",
            80,
            CriterionApplication.None,
            MatchMode.Above,
            0,
            CriterionApplication.All,
            MatchMode.Above,
            false
        )
        val criterionAttempts1below = Criterion(
            "a1-",
            80,
            CriterionApplication.None,
            MatchMode.Below,
            1,
            CriterionApplication.All,
            MatchMode.Below,
            false
        )

        val criterionAttempts2exEq = Criterion(
            "a2=",
            89,
            CriterionApplication.None,
            MatchMode.Equal,
            2,
            CriterionApplication.Once,
            MatchMode.Equal,
            true
        )

        val criterionAttempts3exEq = Criterion(
            "a3=",
            88,
            CriterionApplication.All,
            MatchMode.Equal,
            3,
            CriterionApplication.Once,
            MatchMode.Equal,
            true
        )

        val criterionAttempts1avgEq = Criterion(
            "a1a=",
            94,
            CriterionApplication.None,
            MatchMode.Equal,
            1,
            CriterionApplication.Average,
            MatchMode.Equal,
            true
        )

        val criterionAttempts1avgAbove = Criterion(
            "a1a+",
            90,
            CriterionApplication.None,
            MatchMode.Below,
            1,
            CriterionApplication.Average,
            MatchMode.Above
        )

        val criterionAttempts3Total = Criterion(
            "3t=",
            188,
            CriterionApplication.Total,
            MatchMode.Equal,
            3,
            CriterionApplication.Total,
            MatchMode.Equal
        )

        assertTrue(interactor.matchStudent(student, criterionAttempts1))
        assertFalse(interactor.matchStudent(student, criterionAttempts1below))
        assertTrue(interactor.matchStudent(student, criterionAttempts2exEq))
        assertFalse(interactor.matchStudent(student, criterionAttempts3exEq))
        assertFalse(interactor.matchStudent(student, criterionAttempts1avgEq))
        assertTrue(interactor.matchStudent(student, criterionAttempts1avgAbove))
        assertTrue(interactor.matchStudent(student, criterionAttempts3Total))
    }

    @Test
    fun setToMapTest() {
        val set = setOf<String>("One", "Two", "Three", "Four", "Five")
        set.associate {str -> Pair<String, Int>(str, str.length)}
    }
}