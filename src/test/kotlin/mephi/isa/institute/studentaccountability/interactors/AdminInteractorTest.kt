package mephi.isa.institute.studentaccountability.interactors

import mephi.isa.institute.studentaccountability.util.NotFoundException
import mephi.isa.institute.studentaccountability.model.Role
import mephi.isa.institute.studentaccountability.model.User
import mephi.isa.institute.studentaccountability.repository.InMemoryUserRepository
import mephi.isa.institute.studentaccountability.storage.AuthStorage
import mephi.isa.institute.studentaccountability.util.UnauthorizedException
import org.junit.Test
import mephi.isa.institute.studentaccountability.util.DuplicateKeyException
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class AdminInteractorTest {
    private val authRepository = InMemoryUserRepository(
        mutableMapOf(
            "tutor" to Pair(User("tutor", Role.DepartmentManager, "IT"), "password"),
            "tutor2" to Pair(User("tutor2", Role.DepartmentManager, "IT"), "password"),
            "dean" to Pair(User("dean", Role.InstituteManager), "password")
        ),
        mutableMapOf("admin" to "qwerty")
    )

    private val authStorage = AuthStorage(60 * 60 * 1000)
    private val token: String = authStorage.putAdmin("admin")
    private val interactor = AdminInteractor(authStorage, authRepository)

    @Test
    fun adminAuthTest() {
        val authStorage = AuthStorage(10)
        val interactor = AdminInteractor(authStorage, authRepository)
        val token = interactor.authenticate("admin", "qwerty")
        assertTrue(authStorage.hasAdmin(token))
        Thread.sleep(11)
        assertFalse(authStorage.hasAdmin(token))
        val newToken = interactor.authenticate("admin", "qwerty")
        assertTrue(authStorage.hasAdmin(newToken))
        assertEquals("admin", authStorage.getAdmin(newToken))
        interactor.deauthenticate(newToken)
        assertFalse(authStorage.hasAdmin(newToken))
    }

    @Test(expected = UnauthorizedException::class)
    fun invalidAdminAuthTest() {
        val authStorage = AuthStorage(10)
        val interactor = AdminInteractor(authStorage, authRepository)
        interactor.authenticate("admin0", "qwerty")
    }

    @Test(expected = UnauthorizedException::class)
    fun incorrectDeauthTest() {
        val interactor = AdminInteractor(AuthStorage(10), authRepository)
        interactor.deauthenticate("invalid token")
    }

    @Test(expected = UnauthorizedException::class)
    fun incorrectAuthTest() {
        AuthStorage(10).getAdmin("invalid token")
    }

    @Test
    fun listUserTest() {
        assertEquals(setOf("tutor", "tutor2", "dean"), interactor.listUsers(token).map{u -> u.name}.toSet())
    }

    @Test(expected = UnauthorizedException::class)
    fun unauthorizedListUserTest() {
        interactor.listUsers("invalid token")
    }

    @Test
    fun addUserTest() {
        val user = User("tutor3", Role.DepartmentManager, "Maths")
        interactor.addUser(token, user, "qwe")
        assertEquals(setOf("tutor", "tutor2", "tutor3", "dean"), interactor.listUsers(token).map{u -> u.name}.toSet())
        assertEquals(user, authRepository.auth("tutor3", "qwe"))
    }

    @Test(expected = DuplicateKeyException::class)
    fun duplicateAddUserTest() {
        val user = User("tutor2", Role.DepartmentManager, "Maths")
        interactor.addUser(token, user, "qwe")
    }

    @Test(expected = UnauthorizedException::class)
    fun unauthorizedAddUserTest() {
        val user = User("tutor3", Role.DepartmentManager, "Maths")
        interactor.addUser("invalid token", user, "qwe")
    }

    @Test
    fun updateUserTest() {
        val user = authRepository.auth("dean", "password")
        val newUser = User(user.name, Role.DepartmentManager, "Russian culture")
        interactor.updateUser(token, newUser, "zzz")
        assertEquals(newUser, authRepository.auth("dean", "zzz"))
    }

    @Test(expected = NotFoundException::class)
    fun invalidUpdateUserTest() {
        val newUser = User("someone", Role.DepartmentManager, "Russian culture")
        interactor.updateUser(token, newUser, "zzz")
    }

    @Test(expected = UnauthorizedException::class)
    fun unauthorizedUpdateUserTest() {
        val newUser = User("tutor3", Role.DepartmentManager, "Maths")
        interactor.updateUser("invalid token", newUser, "zzz")
    }

    @Test
    fun removeUserTest() {
        val user = authRepository.auth("tutor", "password")
        interactor.removeUser(token, user)
        assertFalse("tutor" in interactor.listUsers(token).map{u -> u.name})
    }

    @Test(expected = NotFoundException::class)
    fun invalidRemoveUserTest() {
        val newUser = User("someone", Role.DepartmentManager, "Russian culture")
        interactor.removeUser(token, newUser)
    }

    @Test(expected = UnauthorizedException::class)
    fun unauthorizedRemoveUserTest() {
        interactor.removeUser("invalid token", authRepository.loadUser("tutor"))
    }
}