package mephi.isa.institute.studentaccountability.model

import mephi.isa.institute.studentaccountability.util.NotFoundException
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertNull
import kotlin.test.assertTrue

class BusinessClassTest {

    @Test
    fun subjectTest() {
        val subject = Subject("Maths", 2019, 11, false)
        assertEquals(subject.name, "Maths")
        assertEquals(subject.year, 2019)
        assertEquals(subject.month, 11)
        assertEquals(subject.hasExam, false)
        assertEquals(subject.toString(), "Subject Maths at 2019/11")
    }

    @Test(expected = IllegalArgumentException::class)
    fun subjectThrowTest() {
        Subject("Maths", 2019, 13, false)
    }

    @Test(expected = IllegalArgumentException::class)
    fun subjectThrow2Test() {
        Subject("Maths", 2019, 0, false)
    }

    @Test
    fun userTest() {
        val user = User("dean", Role.InstituteManager)
        assertEquals(user.name, "dean")
        assertEquals(user.role, Role.InstituteManager)
        assertNull(user.unit)

        val user2 = User("tutor", Role.DepartmentManager, "Physics")
        assertEquals(user2.name, "tutor")
        assertEquals(user2.role, Role.DepartmentManager)
        assertEquals(user2.unit, "Physics")
    }

    @Test(expected = java.lang.IllegalArgumentException::class)
    fun userInvalidTest() {
        User("tutor", Role.DepartmentManager)
    }

    @Test
    fun studentTest() {
        val math = Subject("Math", 2019, 11, true)
        val history = Subject("History", 2019, 11, false)
        val programming = Subject("Programming", 2019, 10, true)
        val mathProgress = Progress(math, 89, 2)
        val historyProgress = Progress(history, 99, 1)
        val student = Student("John", "Doe", mapOf(math to mathProgress, history to historyProgress))
        assertEquals(student.name, "John")
        assertEquals(student.surname,  "Doe")
        assertEquals(student.marks, mapOf(math to mathProgress, history to historyProgress))
        assertEquals(student.getProgress(math.copy()), mathProgress)
        assertEquals(student.getProgress(history), historyProgress.copy())

        val student2 = Student("Jane", "Doe", mapOf(math to mathProgress, history to historyProgress))
        val student3 = Student("John", "Smith", mapOf(math to mathProgress, history to historyProgress))
        assertEquals(student, student)
        assertEquals(student, student.copy())
        assertTrue(student != student2)
        assertTrue(student != student3)
        val none = null
        assertTrue(student != none)
        val progress: Any? = try {
            student.getProgress(programming)
        } catch (ex: NotFoundException) {
            "None"
        }
        assertEquals(progress, "None")
        assertTrue(student != progress)

    }

    @Test
    fun groupTest() {
        val group = Group("A1-1", 1, "Physics", setOf(Student("John", "Doe", mapOf()), Student("Jane", "Doe", mapOf())))
        assertEquals(group.name, "A1-1")
        assertEquals(group.department, "Physics")
        assertEquals(group.semester, 1)
        assertEquals(group.students, setOf(Student("John", "Doe", mapOf()), Student("Jane", "Doe", mapOf())))
        assertEquals(group, group)
        assertEquals(group, group.copy())
        assertEquals(group.hashCode(), group.copy().hashCode())
        val group2 = Group("A1-2", 1, "Physics", setOf(Student("John", "Doe", mapOf()), Student("Jane", "Doe", mapOf())))
        val group3 =
            Group("A1-1", 1, "Information Security", setOf(Student("John", "Doe", mapOf()), Student("Jane", "Doe", mapOf())))
        val group4 = Group("A1-1", 1, "Physics", setOf())
        val group5 = Group("A1-1", 2, "Physics", setOf())

        assertTrue(group != group2)
        assertTrue(group != group3)
        assertEquals(group, group4)
        assertTrue(group != group5)
        val none = null
        assertTrue(group != none)
        val any: Any = 3
        assertTrue(group != any)
    }

    @Test(expected = IllegalArgumentException::class)
    fun groupInvalidNegTest() {
        Group("A1-1", 0, "Physics", setOf(Student("John", "Doe", mapOf()), Student("Jane", "Doe", mapOf())))
    }

    @Test(expected = IllegalArgumentException::class)
    fun groupInvalidPosTest() {
        Group("A1-1", 15, "Physics", setOf(Student("John", "Doe", mapOf()), Student("Jane", "Doe", mapOf())))
    }

    @Test
    fun criterionTest() {
        val criterion = Criterion(
            "All 5's",
            450,
            CriterionApplication.Total,
            MatchMode.Above,
            1,
            CriterionApplication.Once,
            MatchMode.Equal,
            true
        )
        assertEquals(criterion.name, "All 5's")
        assertEquals(criterion.mark, 450)
        assertEquals(criterion.markMode, CriterionApplication.Total)
        assertEquals(criterion.markMatch, MatchMode.Above)
        assertEquals(criterion.attempts, 1)
        assertEquals(criterion.attemptsMode, CriterionApplication.Once)
        assertEquals(criterion.attemptsMatch, MatchMode.Equal)
        assertTrue(criterion.isExamOnly())
    }

    @Test(expected = IllegalArgumentException::class)
    fun criterionThrowTest() {
        Criterion(
            "All 5's",
            105,
            CriterionApplication.Once,
            MatchMode.Above,
            1,
            CriterionApplication.None,
            MatchMode.Equal,
            true
        )
    }

    @Test(expected = IllegalArgumentException::class)
    fun criterionThrow2Test() {
        Criterion(
            "All 5's",
            -1,
            CriterionApplication.Once,
            MatchMode.Above,
            1,
            CriterionApplication.None,
            MatchMode.Equal,
            true
        )
    }

    @Test(expected = IllegalArgumentException::class)
    fun criterionThrow3Test() {
        Criterion(
            "All 5's",
            90,
            CriterionApplication.Once,
            MatchMode.Above,
            -9,
            CriterionApplication.None,
            MatchMode.Equal,
            true
        )
    }

    @Test(expected = IllegalArgumentException::class)
    fun criterionThrow4Test() {
        Criterion(
            "All 5's",
            -1,
            CriterionApplication.Total,
            MatchMode.Above,
            -9,
            CriterionApplication.None,
            MatchMode.Equal,
            true
        )
    }

    @Test
    fun progressTest() {
        val subject = Subject("Maths", 2019, 11, false)
        val progress = Progress(subject, 100, 1)
        assertEquals(progress.subject, subject)
        assertEquals(progress.composedMark, 100)
        assertEquals(progress.attempts, 1)

        val progress2 = Progress(subject, 7)
        assertEquals(progress2.subject, subject)
        assertEquals(progress2.composedMark, 7)
        assertEquals(progress2.attempts, 0)
    }

    @Test(expected = IllegalArgumentException::class)
    fun progressThrowTest() {
        Progress(Subject("Maths", 2019, 11, false), 101, 1)
    }

    @Test(expected = IllegalArgumentException::class)
    fun progressThrow2Test() {
        Progress(Subject("Maths", 2019, 11, false), -1, 1)
    }

    @Test(expected = IllegalArgumentException::class)
    fun progressThrow3Test() {
        Progress(Subject("Maths", 2019, 11, false), 0, -1)
    }

    @Test
    fun orderListTest() {
        val criterion = Criterion(
            "Any",
            0,
            CriterionApplication.Average,
            MatchMode.Above,
            0,
            CriterionApplication.None,
            MatchMode.Below,
            false
        )
        val orderList = OrderList("Any", criterion.copy(), listOf())
        assertEquals(orderList.name, "Any")
        assertEquals(orderList.criterion, criterion)
        assertEquals(orderList.students, listOf())

        val orderList2 = OrderList("Any2", criterion.copy(), listOf())

        val criterion2 = Criterion(
            "Any2",
            0,
            CriterionApplication.Average,
            MatchMode.Above,
            0,
            CriterionApplication.None,
            MatchMode.Below,
            false
        )
        val orderList3 = OrderList("Any", criterion2, listOf())

        assertEquals(orderList, orderList)
        assertEquals(orderList, orderList.copy())
        assertEquals(orderList.hashCode(), orderList.hashCode())
        assertTrue(orderList != orderList2)
        assertTrue(orderList != orderList3)
        val none = null
        assertTrue(orderList != none)
        val any: Any = "Any"
        assertTrue(orderList != any)
    }
}