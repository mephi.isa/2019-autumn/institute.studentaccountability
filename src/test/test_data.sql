ALTER TABLE order_student DROP CONSTRAINT fk_order_student_student_id_id, ADD CONSTRAINT fk_order_student_student_id_id FOREIGN KEY (student_id)
          REFERENCES student (id) ON DELETE CASCADE;
ALTER TABLE progress DROP CONSTRAINT fk_progress_student_id_id, ADD CONSTRAINT fk_progress_student_id_id FOREIGN KEY (student_id)
          REFERENCES student (id) ON DELETE CASCADE;
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
delete from progress;
delete from student;
delete from subject;
delete from students_group;
delete from department;
delete from accountability_admin;

INSERT INTO accountability_admin (name, password) VALUES('admin', 'XxUVCNM/iYqlze2U64AeVg==');

INSERT INTO department (id, name) VALUES((SELECT uuid_generate_v1()), 'IT');
INSERT INTO department (id, name) VALUES((SELECT uuid_generate_v1()), 'Physics');



INSERT INTO subject (id, name, year, month, exam) VALUES(0, 'Maths', 2019, 12, true);
INSERT INTO subject (id, name, year, month, exam) VALUES(1, 'Physics', 2019, 12, true);
INSERT INTO subject (id, name, year, month, exam) VALUES(2, 'Programming', 2019, 12, true);
INSERT INTO subject (id, name, year, month, exam) VALUES(3, 'Programming', 2019, 12, false);
INSERT INTO subject (id, name, year, month, exam) VALUES(4, 'English', 2019, 12, false);
INSERT INTO subject (id, name, year, month, exam) VALUES(5, 'Tech documentation', 2019, 12, false);


INSERT INTO students_group (id, semester, department_id, name, degree) VALUES((SELECT uuid_generate_v1()), 1, (SELECT id FROM department WHERE name = 'IT'), 'I1-1', 'BACHELOOR');
INSERT INTO students_group (id, semester, department_id, name, degree) VALUES((SELECT uuid_generate_v1()), 1, (SELECT id FROM department WHERE name = 'Physics'), 'P1-1', 'BACHELOOR');


INSERT INTO student (id, name, surname, birth_date, identity_document_number, students_group_id) VALUES ((SELECT uuid_generate_v1()), 'John', 'Doe', '2020-01-08 15:03:29.084', '0', (SELECT id FROM students_group WHERE name = 'I1-1'));
INSERT INTO student (id, name, surname, birth_date, identity_document_number, students_group_id) VALUES ((SELECT uuid_generate_v1()), 'Jane', 'Smith', '2020-01-08 15:03:29.084', '1', (SELECT id FROM students_group WHERE name = 'I1-1'));

INSERT INTO student (id, name, surname, birth_date, identity_document_number, students_group_id) VALUES ((SELECT uuid_generate_v1()), 'Jane', 'Doe', '2020-01-08 15:03:29.084', '2', (SELECT id FROM students_group WHERE name = 'P1-1'));
INSERT INTO student (id, name, surname, birth_date, identity_document_number, students_group_id) VALUES ((SELECT uuid_generate_v1()), 'John', 'Smith', '2020-01-08 15:03:29.084', '3', (SELECT id FROM students_group WHERE name = 'P1-1'));



INSERT INTO progress (student_id, subject_id, mark, attempts) VALUES(
  (SELECT id FROM student WHERE name = 'John' AND surname = 'Doe'),
  0, 90, 1
);
INSERT INTO progress (student_id, subject_id, mark, attempts) VALUES(
  (SELECT id FROM student WHERE name = 'John' AND surname = 'Doe'),
  2, 69, 2
);
INSERT INTO progress (student_id, subject_id, mark, attempts) VALUES(
  (SELECT id FROM student WHERE name = 'John' AND surname = 'Doe'),
  4, 100, 1
);
INSERT INTO progress (student_id, subject_id, mark, attempts) VALUES(
  (SELECT id FROM student WHERE name = 'John' AND surname = 'Doe'),
  5, 84, 1
);

INSERT INTO progress (student_id, subject_id, mark, attempts) VALUES(
  (SELECT id FROM student WHERE name = 'Jane' AND surname = 'Smith'),
  0, 70, 1
);
INSERT INTO progress (student_id, subject_id, mark, attempts) VALUES(
  (SELECT id FROM student WHERE name = 'Jane' AND surname = 'Smith'),
  2, 60, 1
);
INSERT INTO progress (student_id, subject_id, mark, attempts) VALUES(
  (SELECT id FROM student WHERE name = 'Jane' AND surname = 'Smith'),
  4, 90, 1
);
INSERT INTO progress (student_id, subject_id, mark, attempts) VALUES(
  (SELECT id FROM student WHERE name = 'Jane' AND surname = 'Smith'),
  5, 55, 5
);


INSERT INTO progress (student_id, subject_id, mark, attempts) VALUES(
  (SELECT id FROM student WHERE name = 'John' AND surname = 'Smith'),
  0, 95, 1
);
INSERT INTO progress (student_id, subject_id, mark, attempts) VALUES(
  (SELECT id FROM student WHERE name = 'John' AND surname = 'Smith'),
  1, 99, 1
);
INSERT INTO progress (student_id, subject_id, mark, attempts) VALUES(
  (SELECT id FROM student WHERE name = 'John' AND surname = 'Smith'),
  4, 100, 1
);
INSERT INTO progress (student_id, subject_id, mark, attempts) VALUES(
  (SELECT id FROM student WHERE name = 'John' AND surname = 'Smith'),
  3, 92, 1
);

INSERT INTO progress (student_id, subject_id, mark, attempts) VALUES(
  (SELECT id FROM student WHERE name = 'Jane' AND surname = 'Doe'),
  0, 70, 2
);
INSERT INTO progress (student_id, subject_id, mark, attempts) VALUES(
  (SELECT id FROM student WHERE name = 'Jane' AND surname = 'Doe'),
  1, 60, 3
);
INSERT INTO progress (student_id, subject_id, mark, attempts) VALUES(
  (SELECT id FROM student WHERE name = 'Jane' AND surname = 'Doe'),
  4, 75, 3
);
INSERT INTO progress (student_id, subject_id, mark, attempts) VALUES(
  (SELECT id FROM student WHERE name = 'Jane' AND surname = 'Doe'),
  3, 65, 3
);
