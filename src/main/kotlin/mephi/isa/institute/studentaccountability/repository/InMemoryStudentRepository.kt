package mephi.isa.institute.studentaccountability.repository

import mephi.isa.institute.studentaccountability.util.NotFoundException
import mephi.isa.institute.studentaccountability.model.*
import mephi.isa.institute.studentaccountability.util.DuplicateKeyException

class InMemoryStudentRepository(
    private val subjectSet: MutableSet<Subject>,
    private val groupMap: MutableMap<String, Group>,
    private val listMap: MutableMap<String, OrderList>,
    private val criterionMap: MutableMap<String, Criterion>,
    private val deptSet: MutableSet<String>
): IStudentRepository {
    override fun loadSubjects(): Set<Subject> {
        return subjectSet
    }

/*
    override fun addSubjects(subjects: Set<Subject>) {
        for (subject in subjects) {
            if (subject in subjectSet) {
                throw DuplicateKeyException("Subject already exists")
            }
        }
        subjectSet.addAll(subjects)
    }

    override fun loadGroupStudents(group: String): Set<Student> {
        if (group !in groupMap.keys) {
            throw NotFoundException("Group '$group.name' does not exist")
        }

        return groupMap.getValue(group).students
    }
 */

    override fun loadDeptGroups(dept: String): Set<Group> {
        val res = mutableSetOf<Group>()
        for (group in groupMap.values) {
            if (group.department == dept) {
                res.add(group)
            }
        }
        return res
    }

/*
    override fun updateGroups(groups: Set<Group>) {
        for (group in groups) {
            if (group.name !in groupMap.keys) {
                throw NotFoundException("Group '$group.name' does not exist")
            }
        }

        for (group in groups) {
            groupMap[group.name] = group
        }
    }
 */

    override fun loadGroups(): Set<Group> {
        return groupMap.values.toSet()
    }

    override fun loadGroup(name: String): Group {
        if (name !in groupMap.keys) {
            throw NotFoundException("Group '$name' does not exist")
        }

        return groupMap.getValue(name)
    }

/*
    override fun addGroups(groups: Set<Group>) {
        for (group in groups) {
            if (group.name in groupMap.keys) {
                throw DuplicateKeyException("Group '$group.name' already exists")
            }
        }

        for (group in groups) {
            groupMap[group.name] = group
        }
    }
 */

    override fun loadDepts(): Set<String> {
        return deptSet
    }

/*
    override fun addDepts(depts: Set<String>) {
        for (dept in depts) {
            if (dept in deptSet) {
                throw DuplicateKeyException("Department '$dept' already exists")
            }
        }

        deptSet.addAll(depts)
    }
 */

    override fun loadCriteria(): Set<Criterion> {
        return criterionMap.values.toSet()
    }

/*
    override fun updateCriterion(criterion: Criterion) {
        if (criterion.name !in criterionMap.keys) {
            throw NotFoundException("Criterion '$criterion' does not exists")
        }
        criterionMap[criterion.name] = criterion
    }

 */

    override fun addCriterion(criterion: Criterion) {
        if (criterion.name in criterionMap.keys) {
            throw DuplicateKeyException("Criterion '$criterion' already exists")
        }
        criterionMap[criterion.name] = criterion
    }

    override fun loadListNames(): Set<String> {
        return listMap.keys
    }

    override fun loadList(name: String): OrderList {
        if (name !in listMap.keys) {
            throw NotFoundException("List '$name' does not exist")
        }

        return listMap.getValue(name)
    }

    override fun addList(list: OrderList) {
        if (list.name in listMap.keys) {
            throw DuplicateKeyException("Order list '$list.name' already exists")
        }

        listMap[list.name] = list
    }
}