package mephi.isa.institute.studentaccountability.repository

import mephi.isa.institute.studentaccountability.model.*

interface IStudentRepository {

    // Subjects

    fun loadSubjects(): Set<Subject>

//    fun addSubjects(subjects: Set<Subject>)


    // Students

//    fun loadGroupStudents(group: String): Set<Student>

    fun loadDeptGroups(dept: String): Set<Group>

//    fun updateGroups(groups: Set<Group>)


    // Groups

    fun loadGroups(): Set<Group>

//    fun addGroups(groups: Set<Group>)

    fun loadGroup(name: String): Group


    // Depts

    fun loadDepts(): Set<String>

//    fun addDepts(depts: Set<String>)


    // Criteria

    fun loadCriteria(): Set<Criterion>

//    fun updateCriterion(criterion: Criterion)

    fun addCriterion(criterion: Criterion)


    // Lists

    fun loadListNames(): Set<String>

    fun loadList(name: String): OrderList

    fun addList(list: OrderList)

}