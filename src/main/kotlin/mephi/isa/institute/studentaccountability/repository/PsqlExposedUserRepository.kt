package mephi.isa.institute.studentaccountability.repository

import mephi.isa.institute.studentaccountability.model.Role
import mephi.isa.institute.studentaccountability.model.User
import mephi.isa.institute.studentaccountability.repository.OrderT.autoIncrement
import mephi.isa.institute.studentaccountability.repository.UserT.autoIncrement
import mephi.isa.institute.studentaccountability.util.UnauthorizedException
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction

object UserT : Table() {
    val id = integer("id").autoIncrement()
    val name = varchar("name", 255).uniqueIndex()
    val password = varchar("password", 255)
    val role = enumerationByName("role", 32, Role::class)
    val departmentId = (uuid("department_id") references DepartmentT.id).nullable()

    override val tableName: String = "accountability_user"
    override val primaryKey = PrimaryKey(id)
}

object AdminT : Table() {
    val id = integer("id").autoIncrement()
    val name = varchar("name", 255).uniqueIndex()
    val password = varchar("password", 255)

    override val tableName: String = "accountability_admin"
    override val primaryKey = PrimaryKey(id)
}

class PsqlExposedUserRepository(
    uri: String,
    driver: String = "",
    user: String = "",
    password: String = ""
) : IUserRepository {

    val db = Database.connect(uri, driver = driver, user = user, password = password)

    init {
        transaction(db) {
            addLogger(StdOutSqlLogger)

            SchemaUtils.create(UserT, AdminT)
        }
    }

    override fun auth(login: String, password: String): User {
        try {
            val user = transaction(db) {
                (UserT leftJoin DepartmentT).select { UserT.name eq login }.single()
            }

            if (user[UserT.password] != password) {
                throw UnauthorizedException("Password is not correct")
            }

            return User(user[UserT.name], user[UserT.role], user[DepartmentT.name])

        } catch (ex: NoSuchElementException) {
            throw UnauthorizedException("User does not exist")
        }
    }

    override fun authAdmin(login: String, password: String): String {
        try {
            val user = transaction(db) {
                AdminT.select { AdminT.name eq login }.single()
            }

            if (user[AdminT.password] != password) {
                throw UnauthorizedException("Password is not correct")
            }

            return user[AdminT.name]

        } catch (ex: NoSuchElementException) {
            throw UnauthorizedException("User does not exist")
        }
    }

    override fun listUsers(): List<User> {
        return transaction(db) {
            (UserT leftJoin DepartmentT).selectAll().map {
                User(it[UserT.name], it[UserT.role], it[DepartmentT.name])
            }
        }
    }

    override fun loadUser(login: String): User {
        val user = transaction(db) {
            (UserT leftJoin DepartmentT).select { UserT.name eq login }.single()
        }

        return User(user[UserT.name], user[UserT.role], user[DepartmentT.name])
    }

    override fun removeUser(user: User) {
        transaction(db) {
            UserT.deleteWhere { UserT.name eq user.name }
        }
    }

    override fun updateUser(user: User, password: String) {
        transaction(db) {
            addLogger(StdOutSqlLogger)

            val existing = UserT.slice(UserT.id).select { UserT.name eq user.name }
            val dept = if (user.unit != null) {
                DepartmentT.select { DepartmentT.name eq user.unit }.single()[DepartmentT.id]
            } else {
                null
            }

            // homemade insert or update
            if (existing.count() > 0) {
                UserT.update({ UserT.id eq existing.single()[UserT.id] }) {
                    it[name] = user.name
                    it[role] = user.role
                    it[UserT.password] = password
                    it[departmentId] = dept
                }
            } else {
                UserT.insert {
                    it[name] = user.name
                    it[role] = user.role
                    it[UserT.password] = password
                    it[departmentId] = dept
                }
            }
            null
        }
    }
}