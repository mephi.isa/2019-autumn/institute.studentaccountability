package mephi.isa.institute.studentaccountability.repository

import mephi.isa.institute.studentaccountability.model.*
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.`java-time`.datetime
import org.jetbrains.exposed.sql.transactions.transaction
import java.sql.Timestamp

object DepartmentT : Table() {
    val id = uuid("id")
    val name = varchar("name", 255)

    override val tableName: String = "department"
    override val primaryKey = PrimaryKey(id)
}

object GroupT : Table() {
    val id = uuid("id")
    val degree = varchar("degree", 255)
    val name = varchar("name", 255)
    val semester = integer("semester")
    val departmentId = (uuid("department_id") references DepartmentT.id)

    override val tableName: String = "students_group"
    override val primaryKey = PrimaryKey(id)
}

object StudentT : Table() {
    val id = uuid("id")
    val birthDate = datetime("birth_date")
    val personalId = varchar("identity_document_number", 255)
    val name = varchar("name", 255)
    val surname = varchar("surname", 255)
    val studentGroupId = (uuid("students_group_id") references GroupT.id)

    override val tableName: String = "student"
    override val primaryKey = PrimaryKey(id)
}

object SubjectT : Table() {
    val id = integer("id").autoIncrement()
    val name = varchar("name", 255)
    val year = integer("year")
    val month = integer("month")
    val hasExam = bool("exam")

    override val tableName: String = "subject"
    override val primaryKey = PrimaryKey(id)
}

object ProgressT : Table() {
    val student = (uuid("student_id") references StudentT.id)
    val subject = (integer("subject_id") references SubjectT.id)
    val mark = integer("mark")
    val attempts = integer("attempts")

    override val tableName: String = "progress"
    override val primaryKey = PrimaryKey(student, subject)
}

object CriterionT : Table() {
    val id = integer("id").autoIncrement()
    val name = varchar("name", 255).uniqueIndex()
    val mark = integer("mark")
    val markMode = enumerationByName("mark_mode", 16, CriterionApplication::class)
    val markMatch = enumerationByName("mark_match", 16, MatchMode::class)
    val attempts = integer("attempts")
    val attemptsMode = enumerationByName("attempts_mode", 16, CriterionApplication::class)
    val attemptsMatch = enumerationByName("attempts_match", 16, MatchMode::class)
    val requireExams = bool("exams")

    override val tableName: String = "criterion"
    override val primaryKey = PrimaryKey(id)
}

object OrderT : Table() {
    val id = integer("id").autoIncrement()
    val name = varchar("name", 255).uniqueIndex()
    val criterion = (integer("criterion_id") references CriterionT.id)

    override val tableName: String = "order_list"
    override val primaryKey = PrimaryKey(id)
}

object OrderStudent : Table() {
    val student = (uuid("student_id") references StudentT.id)
    val order = (integer("order_id") references OrderT.id)

    override val primaryKey = PrimaryKey(student, order)
    override val tableName: String = "order_student"
}


class PsqlExplosionStudentRepository(
    uri: String,
    driver: String = "",
    user: String = "",
    password: String = ""
) : IStudentRepository {

    val db = Database.connect(uri, driver = driver, user = user, password = password)

    init {
        transaction(db) {
            addLogger(StdOutSqlLogger)

            SchemaUtils.create(GroupT, StudentT, OrderStudent, DepartmentT, CriterionT, OrderT, ProgressT)
        }
    }

    override fun addCriterion(criterion: Criterion) {
        transaction(db) {
            CriterionT.insert {
                it[name] = criterion.name
                it[mark] = criterion.mark
                it[markMode] = criterion.markMode
                it[markMatch] = criterion.markMatch
                it[attempts] = criterion.attempts
                it[attemptsMode] = criterion.attemptsMode
                it[attemptsMatch] = criterion.attemptsMatch
                it[requireExams] = criterion.isExamOnly()
            }
        }
    }

    override fun addList(list: OrderList) {
        transaction(db) {
            val orderId = OrderT.insert {
                it[name] = list.name
                it[criterion] =
                    CriterionT.select { CriterionT.name eq list.criterion.name }.single()[CriterionT.id]
            } get OrderT.id

            OrderStudent.batchInsert(list.students) { pair ->
                this[OrderStudent.order] = orderId
                this[OrderStudent.student] = (StudentT innerJoin GroupT).slice(
                    StudentT.id,
                    StudentT.name,
                    StudentT.surname,
                    GroupT.name
                ).select { (StudentT.name eq pair.first.name) and (
                            StudentT.surname eq pair.first.surname) and (
                            GroupT.name eq pair.second.name) }.single()[StudentT.id]
            }
        }
    }

    override fun loadCriteria(): Set<Criterion> {
        return transaction(db) {
            CriterionT.selectAll().withDistinct().map {
                Criterion(
                    it[CriterionT.name],
                    it[CriterionT.mark],
                    it[CriterionT.markMode],
                    it[CriterionT.markMatch],
                    it[CriterionT.attempts],
                    it[CriterionT.attemptsMode],
                    it[CriterionT.attemptsMatch],
                    it[CriterionT.requireExams]
                )
            }.toSet()
        }
    }

    override fun loadDeptGroups(dept: String): Set<Group> {
        return transaction(db) {
            (DepartmentT innerJoin GroupT).slice(
                DepartmentT.name,
                GroupT.name,
                GroupT.semester,
                GroupT.id,
                GroupT.degree
            ).select { DepartmentT.name eq dept }.withDistinct()
                .map {
                    Group(
                        it[GroupT.name],
                        it[GroupT.semester],
                        it[GroupT.degree],
                        StudentT.select { StudentT.studentGroupId eq it[GroupT.id] }.withDistinct().map { st ->
                            Student(
                                st[StudentT.name],
                                st[StudentT.surname],
                                (ProgressT innerJoin SubjectT).select {
                                    ProgressT.student eq st[StudentT.id]
                                }.withDistinct().associate { pr ->
                                    val subject = Subject(
                                        pr[SubjectT.name],
                                        pr[SubjectT.year],
                                        pr[SubjectT.month],
                                        pr[SubjectT.hasExam]
                                    )
                                    Pair(subject, Progress(subject, pr[ProgressT.mark], pr[ProgressT.attempts]))
                                },
                                Timestamp.valueOf(st[StudentT.birthDate]),
                                st[StudentT.personalId]
                                )
                        }.toSet(),
                        it[DepartmentT.name]
                    )
                }.toSet()
        }
    }

    override fun loadDepts(): Set<String> {
        return transaction(db) {
            DepartmentT.slice(DepartmentT.name).selectAll().withDistinct().map { it[DepartmentT.name] }.toSet()
        }
    }

    override fun loadGroup(name: String): Group {
        return transaction(db) {
            val group = (DepartmentT innerJoin GroupT).slice(
                DepartmentT.name,
                GroupT.name,
                GroupT.semester,
                GroupT.id,
                GroupT.degree
            ).select { GroupT.name eq name }.single()

            Group(
                group[GroupT.name],
                group[GroupT.semester],
                group[DepartmentT.name],
                StudentT.select { StudentT.studentGroupId eq group[GroupT.id] }.withDistinct().map { st ->
                    Student(
                        st[StudentT.name],
                        st[StudentT.surname],
                        (ProgressT innerJoin SubjectT).select {
                            ProgressT.student eq st[StudentT.id]
                        }.withDistinct().associate { pr ->
                            val subject = Subject(
                                pr[SubjectT.name],
                                pr[SubjectT.year],
                                pr[SubjectT.month],
                                pr[SubjectT.hasExam]
                            )
                            Pair(subject, Progress(subject, pr[ProgressT.mark], pr[ProgressT.attempts]))
                        },
                        Timestamp.valueOf(st[StudentT.birthDate]),
                        st[StudentT.personalId])
                }.toSet(),
                group[GroupT.degree]
            )
        }
    }

    override fun loadGroups(): Set<Group> {
        return transaction(db) {
            (DepartmentT innerJoin GroupT).slice(
                DepartmentT.name,
                GroupT.name,
                GroupT.semester,
                GroupT.id,
                GroupT.degree
            ).selectAll().withDistinct()
                .map {
                    Group(
                        it[GroupT.name],
                        it[GroupT.semester],
                        it[DepartmentT.name],
                        StudentT.select { StudentT.studentGroupId eq it[GroupT.id] }.withDistinct().map { st ->
                            Student(
                                st[StudentT.name],
                                st[StudentT.surname],
                                (ProgressT innerJoin SubjectT).select {
                                    ProgressT.student eq st[StudentT.id]
                                }.withDistinct().associate { pr ->
                                    val subject = Subject(
                                        pr[SubjectT.name],
                                        pr[SubjectT.year],
                                        pr[SubjectT.month],
                                        pr[SubjectT.hasExam]
                                    )
                                    Pair(subject, Progress(subject, pr[ProgressT.mark], pr[ProgressT.attempts]))
                                },
                                Timestamp.valueOf(st[StudentT.birthDate]),
                                st[StudentT.personalId])
                        }.toSet(),
                        it[GroupT.degree]
                    )
                }.toSet()
        }
    }

    override fun loadList(name: String): OrderList {
        return transaction(db) {
            val order =
                (OrderT innerJoin CriterionT
                        innerJoin OrderStudent
                        innerJoin StudentT
                        innerJoin GroupT
                        innerJoin DepartmentT).select { OrderT.name eq name }
            if (!order.empty()) {
                val first = order.first()
                OrderList(first[OrderT.name], Criterion(
                    first[CriterionT.name],
                    first[CriterionT.mark],
                    first[CriterionT.markMode],
                    first[CriterionT.markMatch],
                    first[CriterionT.attempts],
                    first[CriterionT.attemptsMode],
                    first[CriterionT.attemptsMatch],
                    first[CriterionT.requireExams]
                ), order.map {
                    Pair(
                        Student(
                            it[StudentT.name],
                            it[StudentT.surname],
                            (ProgressT innerJoin SubjectT).select {
                                ProgressT.student eq it[StudentT.id]
                            }.withDistinct().associate { pr ->
                                val subject = Subject(
                                    pr[SubjectT.name],
                                    pr[SubjectT.year],
                                    pr[SubjectT.month],
                                    pr[SubjectT.hasExam]
                                )
                                Pair(subject, Progress(subject, pr[ProgressT.mark], pr[ProgressT.attempts]))
                            },
                            Timestamp.valueOf(it[StudentT.birthDate]),
                            it[StudentT.personalId]
                        ),
                        Group(
                            it[GroupT.name],
                            it[GroupT.semester],
                            it[DepartmentT.name],
                            setOf(),
                            it[GroupT.degree]
                        )
                    )
                })
            } else {
                val first = (OrderT innerJoin CriterionT).select { OrderT.name eq name }.single()
                OrderList(
                    first[OrderT.name], Criterion(
                        first[CriterionT.name],
                        first[CriterionT.mark],
                        first[CriterionT.markMode],
                        first[CriterionT.markMatch],
                        first[CriterionT.attempts],
                        first[CriterionT.attemptsMode],
                        first[CriterionT.attemptsMatch],
                        first[CriterionT.requireExams]
                    ), listOf()
                )
            }
        }
    }

    override fun loadListNames(): Set<String> {
        return transaction(db) {
            OrderT.slice(OrderT.name).selectAll().withDistinct().map { it[OrderT.name] }.toSet()
        }
    }

    override fun loadSubjects(): Set<Subject> {
        return transaction(db) {
            SubjectT.selectAll().map {
                Subject(
                    it[SubjectT.name],
                    it[SubjectT.year],
                    it[SubjectT.month],
                    it[SubjectT.hasExam]
                )
            }.toSet()
        }
    }
}