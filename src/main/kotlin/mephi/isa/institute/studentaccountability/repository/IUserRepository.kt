package mephi.isa.institute.studentaccountability.repository

import mephi.isa.institute.studentaccountability.model.*

interface IUserRepository {

    // Auth

    fun auth(login: String, password: String): User

    fun authAdmin(login: String, password: String): String

    // Users

    fun loadUser(login: String): User

    fun updateUser(user: User, password: String)

    fun removeUser(user: User)

    fun listUsers(): List<User>

}