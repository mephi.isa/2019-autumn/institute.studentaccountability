package mephi.isa.institute.studentaccountability.repository

import mephi.isa.institute.studentaccountability.util.NotFoundException
import mephi.isa.institute.studentaccountability.model.User
import mephi.isa.institute.studentaccountability.util.UnauthorizedException

class InMemoryUserRepository(
    private val userMap: MutableMap<String, Pair<User, String>>,
    private val adminMap: MutableMap<String, String>
): IUserRepository {
    override fun auth(login: String, password: String): User {
        if (!userMap.containsKey(login)) {
            throw UnauthorizedException("User does not exist")
        }
        if (userMap.getValue(login).second != password) {
            throw UnauthorizedException("Password is not correct")
        }

        return userMap.getValue(login).first
    }

    override fun authAdmin(login: String, password: String): String {
        if (!adminMap.containsKey(login)) {
            throw UnauthorizedException("User does not exist")
        }
        if (adminMap.getValue(login) != password) {
            throw UnauthorizedException("Password is not correct")
        }

        return login
    }

    override fun loadUser(login: String): User {
        if (!userMap.containsKey(login)) {
            throw UnauthorizedException("User does not exist")
        }

        return userMap.getValue(login).first
    }

    override fun updateUser(user: User, password: String) {
        userMap[user.name] = Pair(user, password)
    }

    override fun removeUser(user: User) {
        if (!userMap.containsKey(user.name)) {
            throw NotFoundException("User does not exist")
        }
        userMap.remove(user.name)
    }

    override fun listUsers(): List<User> {
        return userMap.values.toList().map{u -> u.first}
    }
}