package mephi.isa.institute.studentaccountability.util

data class StudentListing(
    val name: String,
    val surname: String,
    val birthDate: String?,
    val identityDocumentNumber: String?
)