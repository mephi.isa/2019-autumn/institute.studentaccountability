package mephi.isa.institute.studentaccountability.util

data class DepartmentListing(
    val name: String,
    val studentsGroups: List<GroupListing>
)