package mephi.isa.institute.studentaccountability.util

data class GroupListing(
    val name: String,
    val semester: Int,
    val degree: String?,
    val students: List<StudentListing>
)