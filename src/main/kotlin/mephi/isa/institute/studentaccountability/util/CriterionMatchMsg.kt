package mephi.isa.institute.studentaccountability.util

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CriterionMatchMsg(
    val name: String,
    val criterion: String,
    val groups: List<String>
)