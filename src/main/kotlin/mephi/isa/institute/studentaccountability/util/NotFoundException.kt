package mephi.isa.institute.studentaccountability.util

import java.lang.Exception

class NotFoundException(message: String): Exception(message)