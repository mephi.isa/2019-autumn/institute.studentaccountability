package mephi.isa.institute.studentaccountability.util

import com.google.gson.Gson
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import java.io.IOError
import java.lang.reflect.Type


class MoshiGsonWrapper(val moshi: Moshi, val gson: Gson = Gson()) {

    inline fun<reified T> toJson(value: T): String {
        return gson.toJson(value)
    }

    fun <T> fromJson(json: String, typeOfT: Class<T>): T {
        val jsonAdapter = moshi.adapter<T>(typeOfT as Type)
        return jsonAdapter.fromJson(json) ?: throw IOError(null)
    }

}