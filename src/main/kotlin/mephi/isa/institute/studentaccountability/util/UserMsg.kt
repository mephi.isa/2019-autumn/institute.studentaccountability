package mephi.isa.institute.studentaccountability.util

import com.squareup.moshi.JsonClass
import mephi.isa.institute.studentaccountability.model.User

@JsonClass(generateAdapter = true)
data class UserMsg(
    val user: User,
    val password: String
)