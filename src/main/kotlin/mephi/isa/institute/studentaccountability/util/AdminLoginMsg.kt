package mephi.isa.institute.studentaccountability.util

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AdminLoginMsg(
    val login: String,
    val password: String
)