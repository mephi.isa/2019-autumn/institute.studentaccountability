package mephi.isa.institute.studentaccountability.util

import java.lang.Exception

class DuplicateKeyException(message: String): Exception(message)