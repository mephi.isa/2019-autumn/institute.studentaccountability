package mephi.isa.institute.studentaccountability.util

import java.lang.Exception

class UnauthorizedException(message: String): Exception(message)