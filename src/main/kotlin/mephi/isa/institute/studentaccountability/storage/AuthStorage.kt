package mephi.isa.institute.studentaccountability.storage

import mephi.isa.institute.studentaccountability.model.User
import mephi.isa.institute.studentaccountability.util.UnauthorizedException
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ThreadLocalRandom
import kotlin.math.max
import kotlin.random.Random
import kotlin.streams.asSequence

class AuthStorage (private val expiration: Long) {
    private val tokenMap: ConcurrentHashMap<String, Pair<Long, User>> = ConcurrentHashMap()
    private val adminTokenMap: ConcurrentHashMap<String, Pair<Long, String>> = ConcurrentHashMap()

    private fun clearExpired() {
        val now = System.currentTimeMillis()
        for (key in tokenMap.keys) {
            if (now - tokenMap[key]!!.first > expiration) {
                tokenMap.remove(key)
            }
        }
        for (key in adminTokenMap.keys) {
            if (now - adminTokenMap[key]!!.first > expiration) {
                adminTokenMap.remove(key)
            }
        }
    }

    private fun generateToken(): String {
        return ThreadLocalRandom.current()
            .ints(STRING_LENGTH.toLong(), 0 , charPool.size)
            .asSequence()
            .map(charPool::get)
            .joinToString("")
    }

    fun putUser(user: User): String {
        clearExpired()
        val token = generateToken()
        tokenMap[token] = Pair(System.currentTimeMillis(), user)
        return token
    }

    fun putAdmin(user: String): String {
        clearExpired()
        val token = generateToken()
        adminTokenMap[token] = Pair(System.currentTimeMillis(), user)
        return token
    }

    fun logout(token: String) {
        clearExpired()
        tokenMap.remove(token)
    }

    fun logoutAdmin(token: String) {
        clearExpired()
        adminTokenMap.remove(token)
    }

    fun hasUser(token: String): Boolean {
        clearExpired()
        return tokenMap.containsKey(token)
    }

    fun getUser(token: String): User {
        clearExpired()
        if (!hasUser(token)) {
            throw UnauthorizedException("Token has expired")
        }
        return tokenMap.getValue(token).second
    }

    fun hasAdmin(token: String): Boolean {
        clearExpired()
        return adminTokenMap.containsKey(token)
    }

    fun getAdmin(token: String): String {
        clearExpired()
        if (!hasAdmin(token)) {
            throw UnauthorizedException("Token has expired")
        }
        return adminTokenMap.getValue(token).second
    }

    companion object {
        private val charPool : List<Char> = ('a'..'z') + ('A'..'Z') + ('0'..'9') +('+') + ('/')
        private const val STRING_LENGTH = 24
    }
}