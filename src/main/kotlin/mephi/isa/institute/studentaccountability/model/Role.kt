package mephi.isa.institute.studentaccountability.model

enum class Role {
    InstituteManager,
    DepartmentManager,
}