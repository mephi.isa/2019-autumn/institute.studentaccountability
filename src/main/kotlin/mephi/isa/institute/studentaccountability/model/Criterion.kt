package mephi.isa.institute.studentaccountability.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Criterion (
    val name: String,
    val mark: Int,
    val markMode: CriterionApplication,
    val markMatch: MatchMode,
    val attempts: Int,
    val attemptsMode: CriterionApplication,
    val attemptsMatch: MatchMode,
    private val requireExams: Boolean = false
) {
    init {
        if (markMode == CriterionApplication.Total) {
            require(mark > 0)
        } else {
            require(mark in (0..101))
        }
        require(attempts >= 0)
    }

    fun isExamOnly(): Boolean {
        return requireExams
    }
}