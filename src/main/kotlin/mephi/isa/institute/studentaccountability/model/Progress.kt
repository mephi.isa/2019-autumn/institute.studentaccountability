package mephi.isa.institute.studentaccountability.model

data class Progress  (
    val subject: Subject,
    val composedMark: Int,
    val attempts: Int = 0
) {
    init {
        require(composedMark in 0..100)
        require(attempts >= 0)
    }
}