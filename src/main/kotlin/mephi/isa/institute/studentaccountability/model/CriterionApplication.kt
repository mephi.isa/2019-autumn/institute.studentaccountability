package mephi.isa.institute.studentaccountability.model

enum class CriterionApplication {
    None,
    Once,
    Average,
    All,
    Total
}