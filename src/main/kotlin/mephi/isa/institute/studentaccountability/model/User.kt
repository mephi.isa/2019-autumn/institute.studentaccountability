package mephi.isa.institute.studentaccountability.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class User(
    val name: String,
    val role: Role,
    val unit: String? = null
) {
    init {
        require(role == Role.InstituteManager || unit != null)
    }
}

