package mephi.isa.institute.studentaccountability.model

enum class MatchMode {
    Below,
    Equal,
    Above
}