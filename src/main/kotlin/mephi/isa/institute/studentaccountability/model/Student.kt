package mephi.isa.institute.studentaccountability.model

import mephi.isa.institute.studentaccountability.util.NotFoundException

data class Student(
    val name: String,
    val surname: String,
    val marks: Map<Subject, Progress>,
    val birthDate: java.sql.Timestamp? = null,
    val identityDocumentNumber: String? = null
) {

    fun getProgress(subject: Subject): Progress {
        if (subject in marks) {
            return marks.getValue(subject)
        } else {
            throw NotFoundException("Subject $subject not found")
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Student

        if (name != other.name) return false
        if (surname != other.surname) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + surname.hashCode()
        return result
    }

}