package mephi.isa.institute.studentaccountability.model

data class OrderList (
    val name: String,
    val criterion: Criterion,
    val students: List<Pair<Student, Group>>
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as OrderList

        if (name != other.name) return false
        if (criterion != other.criterion) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + criterion.hashCode()
        return result
    }

}