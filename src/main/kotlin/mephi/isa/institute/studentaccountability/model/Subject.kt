package mephi.isa.institute.studentaccountability.model

data class Subject(
    val name: String,
    val year: Int,
    val month: Int,
    val hasExam: Boolean
) {
    init {
        require(month in 1..12)
    }

    override fun toString(): String {
        return "Subject $name at $year/$month"
    }
}