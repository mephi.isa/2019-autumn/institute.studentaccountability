package mephi.isa.institute.studentaccountability.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Group (
    val name: String,
    val semester: Int,
    val department: String,
    val students: Set<Student>,
    val degree: String? = null
) {

    init {
        require(semester in 1..14)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Group

        if (name != other.name) return false
        if (semester != other.semester) return false
        if (department != other.department) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + semester
        result = 31 * result + department.hashCode()
        return result
    }
}