package mephi.isa.institute.studentaccountability

import com.squareup.moshi.JsonDataException
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import mephi.isa.institute.studentaccountability.interactors.AccountabilityInteractor
import mephi.isa.institute.studentaccountability.interactors.AdminInteractor
import mephi.isa.institute.studentaccountability.interactors.AuthInteractor
import mephi.isa.institute.studentaccountability.model.*
import mephi.isa.institute.studentaccountability.repository.*
import mephi.isa.institute.studentaccountability.storage.AuthStorage
import mephi.isa.institute.studentaccountability.util.*
import org.eclipse.jetty.util.log.Log
import spark.Spark.*
import java.io.IOException
import java.lang.IllegalArgumentException
import java.lang.reflect.InvocationTargetException
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.NoSuchElementException

fun main(args: Array<String>) {
    if ("--help" in args || "-h" in args) {
        println("Usage: SERVER db_type host:port/database login password")
        return
    }

    if (args.size < 4) {
        throw IllegalArgumentException("argv[] does not contain enough arguments: only ${args.size}")
    }
    val expiration: Long = 24 * 60 * 60

    val authRepository: IUserRepository = PsqlExposedUserRepository(
        "jdbc:${args[0]}://${args[1]}", driver = "org.${args[0]}.Driver",
        user = args[2], password = args[3]
    )

    val authStorage = AuthStorage(expiration * 1000)
    val authInteractor = AuthInteractor(authStorage, authRepository)
    val adminInteractor = AdminInteractor(authStorage, authRepository)

    val studentRepository: IStudentRepository = PsqlExplosionStudentRepository(
        "jdbc:${args[0]}://${args[1]}", driver = "org.${args[0]}.Driver",
        user = args[2], password = args[3]
    )

    val accountabilityInteractor = AccountabilityInteractor(authStorage, studentRepository)

    val logger = Log.getLogger("SaServer")
    port(5088)

    staticFiles.location("/public")

    exception(Exception::class.java) { e, req, res ->
        res.status(500); res.body("Error"); logger.info(
        "Error @${req.pathInfo()}: $e",
        e
    )
    }

    val coursePassedCriterion = Criterion(
        "Course passed",
        59,
        CriterionApplication.All,
        MatchMode.Above,
        0,
        CriterionApplication.All,
        MatchMode.Above,
        false
    )

    val courseFailedCriterion = Criterion(
        "Course failed",
        60,
        CriterionApplication.Once,
        MatchMode.Below,
        0,
        CriterionApplication.None,
        MatchMode.Above,
        false
    )

    if (coursePassedCriterion !in studentRepository.loadCriteria()) {
        studentRepository.addCriterion(coursePassedCriterion)
    }

    if (courseFailedCriterion !in studentRepository.loadCriteria()) {
        studentRepository.addCriterion(courseFailedCriterion)
    }

    val gs = MoshiGsonWrapper(
        Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()
    )

    exception(UnauthorizedException::class.java) { e, req, res ->
        res.status(403)
        res.body(gs.toJson("403: Permission denied to access this data with your credentials now, try log in again"))
        logger.info("Unauthorized @${req.pathInfo()}: $e", e)
    }

    exception(DuplicateKeyException::class.java) { e, req, res ->
        res.status(400)
        res.body(gs.toJson("400: Requested value already exists"))
        logger.info("Duplicate @${req.pathInfo()}: $e", e)
    }

    exception(NoSuchElementException::class.java) { e, req, res ->
        res.status(400)
        res.body(gs.toJson("400: Requested value does not exists"))
        logger.info("Not Found (element) @${req.pathInfo()}: $e", e)
    }

    exception(NotFoundException::class.java) { e, req, res ->
        res.status(400)
        res.body(gs.toJson("400: Requested value does not exists"))
        logger.info("Not Found @${req.pathInfo()}: $e", e)
    }

    exception(JsonDataException::class.java) { e, req, res ->
        res.status(400)
        res.body(gs.toJson("400: Bad JSON format"))
        logger.info("BadRequest (json format) @${req.pathInfo()}: $e", e)
    }

    exception(IOException::class.java) { e, req, res ->
        res.status(400)
        res.body(gs.toJson("400: Bad JSON"))
        logger.info("BadRequest (json) @${req.pathInfo()}: $e", e)
    }

    exception(InvocationTargetException::class.java) { e, req, res ->
        res.status(400)
        res.body(gs.toJson("400: Bad JSON content"))
        logger.info("BadRequest (json content) @${req.pathInfo()}: $e", e)
    }

    exception(TypeCastException::class.java) { e, req, res ->
        res.status(400)
        res.body(gs.toJson("400: Bad JSON types used"))
        logger.info("BadRequest (incomplete class) @${req.pathInfo()}: $e", e)
    }

    exception(IllegalStateException::class.java) { e, req, res ->
        if (e.message != null && e.message!!.contains("cookie")) {
            if (e.message!!.contains("token")) {
                res.status(400)
                res.body(gs.toJson("403: No credentials found, try log in again"))
                logger.info("Unauthorized @${req.pathInfo()}: $e", e)
            } else {
                res.status(400)
                res.body(gs.toJson("400: Bad cookie"))
                logger.info("Bad request (cookie) @${req.pathInfo()}: $e", e)
            }
        } else {
            res.status(500)
            res.body(gs.toJson("500: Internal server error"))
            logger.info("Illegal error @${req.pathInfo()}: $e", e)
        }

    }

    staticFiles.location("/public")

    path("/api/v0") {

        path("/auth") {
            post("/login") { req, res ->
                val adminMsg = gs.fromJson(req.body(), AdminLoginMsg::class.java)
                val token: String =
                    authInteractor.authenticate(adminMsg.login, adminMsg.password)
                res.header("Set-Cookie", "token=$token; Max-Age=$expiration; Path=/; SameSite=None")
                res.header("Access-Control-Allow-Origin", "*")
                gs.toJson(authStorage.getUser(token))
            }

            post("/logout") { req, res ->
                authInteractor.deauthenticate(req.cookie("token"))
                res.status(200)
                gs.toJson("Logged out")
            }

            get("/verify") { req, res ->
                res.status(200)
                gs.toJson(authInteractor.verify(gs.fromJson(req.body(), String::class.java)))
            }
        }

        path("/admin") {
            post("/login") { req, res ->
                val adminMsg = gs.fromJson(req.body(), AdminLoginMsg::class.java)
                val token: String = adminInteractor.authenticate(adminMsg.login, adminMsg.password)
                res.header("Set-Cookie", "token=$token; Max-Age=$expiration; Path=/; SameSite=None")
                res.header("Access-Control-Allow-Origin", "*")
                gs.toJson("Logged in")
            }

            post("/logout") { req, res ->
                adminInteractor.deauthenticate(req.cookie("token"))
                res.status(200)
                gs.toJson("Logged out")
            }

            get("/users") { req, res ->
                val list = adminInteractor.listUsers(req.cookie("token"))
                res.status(200)
                gs.toJson(list)
            }

            post("/users/update") { req, res ->
                val userMsg = gs.fromJson(req.body(), UserMsg::class.java)
                val user: User = userMsg.user
                val password: String = userMsg.password
                adminInteractor.updateUser(req.cookie("token"), user, password)
                res.status(200)
                gs.toJson("Updated")
            }

            post("/users/create") { req, res ->
                val userMsg = gs.fromJson(req.body(), UserMsg::class.java)
                val user: User = userMsg.user
                val password: String = userMsg.password
                adminInteractor.addUser(req.cookie("token"), user, password)
                res.status(200)
                gs.toJson("Added")
            }

            post("/users/remove") { req, res ->
                val user = gs.fromJson(req.body(), User::class.java)
                adminInteractor.removeUser(req.cookie("token"), user)
                res.status(200)
                gs.toJson("Removed")
            }
        }

        path("/edu") {
            get("/depts") { req, res ->
                val list = accountabilityInteractor.listDepts(req.cookie("token"))
                res.status(200)
                gs.toJson(list)
            }

            get("/groups") { req, res ->
                val list = accountabilityInteractor.listGroups(req.cookie("token"))
                res.status(200)
                gs.toJson(list)
            }

            get("/groups/:group") { req, res ->
                val list = accountabilityInteractor.getGroup(req.cookie("token"), req.params(":group"))
                res.status(200)
                gs.toJson(list)
            }

            get("/criteria") { req, res ->
                val list = accountabilityInteractor.listCriteria(req.cookie("token"))
                res.status(200)
                gs.toJson(list)
            }

            get("/criterion/:name") { req, res ->
                val list = accountabilityInteractor.getCriterion(req.cookie("token"), req.params(":name"))
                res.status(200)
                gs.toJson(list)
            }

            post("/criteria/new") { req, res ->
                val criterion = gs.fromJson(req.body(), Criterion::class.java)
                accountabilityInteractor.addCriterion(req.cookie("token"), criterion)
                res.status(200)
                gs.toJson("Created")
            }

            get("/orders") { req, res ->
                val list = accountabilityInteractor.listOrderLists(req.cookie("token"))
                res.status(200)
                gs.toJson(list)
            }

            get("/order/:order") { req, res ->
                val list = accountabilityInteractor.getOrderList(req.cookie("token"), req.params(":order"))
                res.status(200)
                gs.toJson(list)
            }

            post("/orders/new") { req, res ->
                val matchMsg = gs.fromJson(req.body(), CriterionMatchMsg::class.java)
                val name = matchMsg.name
                val criterion = matchMsg.criterion
                val groups = matchMsg.groups
                accountabilityInteractor.createOrder(req.cookie("token"), name, criterion, groups.toSet())
                res.status(200)
                gs.toJson("Created")
            }
        }

        path("/ext") {
            path("/students") {
                get("/by_dept") { req, res ->
                    val token: String = req.cookie("token")
                    val groups = accountabilityInteractor.listGroups(token)
                        .map { group -> accountabilityInteractor.getGroup(token, group) }
                    gs.toJson(accountabilityInteractor.listDepts(token).map { dept ->
                        DepartmentListing(
                            dept,
                            groups.filter { group -> group.department == dept }.map { group ->
                                GroupListing(
                                    group.name,
                                    group.semester,
                                    group.degree,
                                    group.students.map { student ->
                                        StudentListing(
                                            student.name,
                                            student.surname,
                                            if (student.birthDate == null) {
                                                null
                                            } else {
                                                student.birthDate.toLocalDateTime()
                                                    .format(DateTimeFormatter.ISO_LOCAL_DATE)
                                            },
                                            student.identityDocumentNumber
                                        )
                                    }
                                )
                            }
                        )
                    })
                }

                get("/for_exclusion") { req, res ->
                    val token: String = req.cookie("token")
                    val date = Calendar.getInstance()
                    val orderName =
                        "for_exclusion_${date.get(Calendar.YEAR)}_${date.get(Calendar.MONTH)}_${date.get(Calendar.DAY_OF_YEAR)}"
                    val orders = accountabilityInteractor.listOrderLists(token)
                    val groups = accountabilityInteractor.listGroups(token)
                    if (orderName !in orders) {
                        accountabilityInteractor.createOrder(token, orderName, courseFailedCriterion.name, groups)
                    }
                    val orderList = accountabilityInteractor.getOrderList(token, orderName)
                    val groupListings: MutableList<GroupListing> = mutableListOf()
                    for (groupName in groups) {
                        val studentList =
                            orderList.students.toList().filter { student -> student.second.name == groupName }
                                .map { student ->
                                    StudentListing(
                                        student.first.name,
                                        student.first.surname,
                                        if (student.first.birthDate == null) {
                                            null
                                        } else {
                                            student.first.birthDate!!.toLocalDateTime()
                                                .format(DateTimeFormatter.ISO_LOCAL_DATE)
                                        },
                                        student.first.identityDocumentNumber
                                    )
                                }
                        if (studentList.isNotEmpty()) {
                            val group = accountabilityInteractor.getGroup(token, groupName)
                            groupListings.add(GroupListing(groupName, group.semester, group.degree, studentList))
                        }
                    }
                    res.status(200)
                    gs.toJson(groupListings)
                }

                get("/to_transfer/:group/:name/:surname") { req, res ->
                    val token: String = req.cookie("token")
                    val group = req.params(":group")
                    val name = req.params(":name")
                    val surname = req.params(":surname")
                    val date = Calendar.getInstance()
                    val orderName =
                        "to_transfer_${date.get(Calendar.YEAR)}_${date.get(Calendar.MONTH)}_${date.get(Calendar.DAY_OF_YEAR)}"
                    val orders = accountabilityInteractor.listOrderLists(token)
                    val groups = accountabilityInteractor.listGroups(token)
                    if (orderName !in orders) {
                        accountabilityInteractor.createOrder(token, orderName, coursePassedCriterion.name, groups)
                    }
                    val orderList = accountabilityInteractor.getOrderList(token, orderName)
                    res.status(200)
                    gs.toJson(orderList.students.count { studentPair -> studentPair.first.name == name && studentPair.first.surname == surname && studentPair.second.name == group } != 0)
                }
            }
        }
    }
}
