package mephi.isa.institute.studentaccountability.interactors

import mephi.isa.institute.studentaccountability.repository.IUserRepository
import mephi.isa.institute.studentaccountability.storage.AuthStorage
import mephi.isa.institute.studentaccountability.util.UnauthorizedException

class AuthInteractor(private val authStorage: AuthStorage, private val authRepository: IUserRepository) {
    fun authenticate(login: String, password: String): String {
        val user = authRepository.auth(login, password)
        return authStorage.putUser(user)
    }

    fun deauthenticate(token: String) {
        if (!authStorage.hasUser(token)) {
            throw UnauthorizedException("Token has expired")
        }

        authStorage.logout(token)
    }

    fun verify(token: String): Boolean {
        return authStorage.hasUser(token)
    }
}