package mephi.isa.institute.studentaccountability.interactors

import mephi.isa.institute.studentaccountability.model.*
import mephi.isa.institute.studentaccountability.repository.IStudentRepository
import mephi.isa.institute.studentaccountability.repository.InMemoryStudentRepository
import mephi.isa.institute.studentaccountability.storage.AuthStorage
import mephi.isa.institute.studentaccountability.util.NotFoundException
import mephi.isa.institute.studentaccountability.util.UnauthorizedException
import kotlin.math.abs

class AccountabilityInteractor(
    private val authStorage: AuthStorage,
    private val studentRepository: IStudentRepository
) {

    // GET

    fun listDepts(token: String): Set<String> {
        if (!authStorage.hasUser(token) && !authStorage.hasAdmin(token)) {
            throw UnauthorizedException("Token has expired")
        }

        return studentRepository.loadDepts()
    }

    fun listGroups(token: String): Set<String> {
        val user = authStorage.getUser(token)

        val res =
            if (user.role == Role.DepartmentManager) {
                studentRepository.loadDeptGroups(user.unit!!)
            } else {
                studentRepository.loadGroups()
            }
        return res.mapTo(mutableSetOf(), { group -> group.name })
    }

    fun listAllStudents(token: String): List<Triple<String, String, String>> {
        val user = authStorage.getUser(token)

        val groups =
            if (user.role == Role.DepartmentManager) {
                studentRepository.loadDeptGroups(user.unit!!)
            } else {
                studentRepository.loadGroups()
            }

        val result = mutableListOf<Triple<String, String, String>>()
        for (group in groups) {
            for (student in group.students) {
                result.add(Triple(student.name, student.surname, group.name))
            }
        }

        return result
    }

    fun listGroupStudents(token: String, groupName: String): List<Triple<String, String, String>> {
        val group = getGroup(token, groupName)

        val result = mutableListOf<Triple<String, String, String>>()
        for (student in group.students) {
            result.add(Triple(student.name, student.surname, group.name))
        }
        return result
    }

    fun listCriteria(token: String): Set<String> {
        if (!authStorage.hasUser(token)) {
            throw UnauthorizedException("Token has expired")
        }

        return studentRepository.loadCriteria().mapTo(mutableSetOf(), { cr -> cr.name })
    }

    fun listOrderLists(token: String): Set<String> {
        if (!authStorage.hasUser(token)) {
            throw UnauthorizedException("Token has expired")
        }

        return studentRepository.loadListNames()
    }

    fun getGroup(token: String, groupName: String): Group {
        val user = authStorage.getUser(token)

        val group = studentRepository.loadGroup(groupName)

        if (user.role == Role.DepartmentManager
            && group.department != user.unit!!
        ) {
            throw UnauthorizedException("No group named '$groupName' available")
        }

        return group
    }

    fun getCriterion(token: String, name: String): Criterion {
        if (!authStorage.hasUser(token)) {
            throw UnauthorizedException("Token has expired")
        }

        return studentRepository.loadCriteria().first { cr -> cr.name == name }
    }

    fun getOrderList(token: String, name: String): OrderList {
        val user = authStorage.getUser(token)

        val orderList = studentRepository.loadList(name)

        if (user.role == Role.DepartmentManager
            && orderList.students.any { pair -> pair.second.department != user.unit!! }
        ) {
            throw UnauthorizedException("No order list named '$name' available")
        }

        return orderList
    }

    // PUT

    fun addCriterion(token: String, criterion: Criterion) {
        if (!authStorage.hasUser(token)) {
            throw UnauthorizedException("Token has expired")
        }

        studentRepository.addCriterion(criterion)
    }

    fun createOrder(token: String, name: String, criterionName: String, groups: Set<String>) {
        val user = authStorage.getUser(token)

        val criterion = try {
            studentRepository.loadCriteria().first { cr -> cr.name == criterionName }
        } catch (nse: NoSuchElementException) {
            if (nse.message != null) throw NotFoundException(nse.message!!) else throw NotFoundException("null")
        }

        val studentList = mutableListOf<Pair<Student, Group>>()

        for (groupName in groups) {
            val group = studentRepository.loadGroup(groupName)
            if (user.role == Role.DepartmentManager
                && group.department != user.unit
            ) {
                throw UnauthorizedException("No group named '$groupName' available")
            }
            for (student in group.students) {
                if (matchStudent(student, criterion)) {
                    studentList.add(Pair(student, purifyGroup(group)))
                }
            }
        }

        studentRepository.addList(OrderList(name, criterion, studentList))
    }


    internal fun matchStudent(student: Student, criterion: Criterion): Boolean {
        val filteredMarks = if (criterion.isExamOnly()) {
            student.marks.filter { progress -> progress.key.hasExam }
        } else {
            student.marks
        }

        var comparator = when (criterion.markMatch) {
            MatchMode.Equal -> { x: Double, y: Int -> abs(x - y.toDouble()) < 0.00001 }
            MatchMode.Below -> { x: Double, y: Int -> x < y.toDouble() }
            MatchMode.Above -> { x: Double, y: Int -> x > y.toDouble() }
        }

        val markPass: Boolean = when (criterion.markMode) {
            CriterionApplication.None -> true
            CriterionApplication.All -> {
                var res = true
                filteredMarks.forEach { mark ->
                    res = res && comparator(mark.value.composedMark.toDouble(), criterion.mark)
                }
                res
            }
            CriterionApplication.Once -> {
                var res = false
                filteredMarks.forEach { mark ->
                    res = res || comparator(mark.value.composedMark.toDouble(), criterion.mark)
                }
                res
            }
            CriterionApplication.Average -> {
                comparator(filteredMarks.map { progress -> progress.value.composedMark }.average(), criterion.mark)
            }
            CriterionApplication.Total -> {
                comparator(
                    filteredMarks.map { progress -> progress.value.composedMark }.sum().toDouble(),
                    criterion.mark
                )
            }
        }


        comparator = when (criterion.attemptsMatch) {
            MatchMode.Equal -> { x: Double, y: Int -> (x - y.toDouble()) < 0.00001 }
            MatchMode.Below -> { x: Double, y: Int -> x < y.toDouble() }
            MatchMode.Above -> { x: Double, y: Int -> x > y.toDouble() }
        }

        val attemptPass: Boolean = when (criterion.attemptsMode) {
            CriterionApplication.None -> true
            CriterionApplication.All -> {
                var res = true
                for (mark in filteredMarks) {
                    res = res && comparator(mark.value.attempts.toDouble(), criterion.attempts)
                }
                res
            }
            CriterionApplication.Once -> {
                var res = false
                for (mark in filteredMarks) {
                    res = res || comparator(mark.value.attempts.toDouble(), criterion.attempts)
                }
                res
            }
            CriterionApplication.Average -> {
                comparator(filteredMarks.map { progress -> progress.value.attempts }.average(), criterion.attempts)
            }
            CriterionApplication.Total -> {
                comparator(
                    filteredMarks.map { progress -> progress.value.attempts }.sum().toDouble(),
                    criterion.attempts
                )
            }
        }


        return attemptPass && markPass
    }

    private fun purifyGroup(group: Group): Group {
        return Group(group.name, group.semester, group.department, setOf())
    }
}