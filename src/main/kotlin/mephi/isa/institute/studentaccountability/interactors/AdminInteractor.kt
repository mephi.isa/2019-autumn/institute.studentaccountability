package mephi.isa.institute.studentaccountability.interactors

import mephi.isa.institute.studentaccountability.util.NotFoundException
import mephi.isa.institute.studentaccountability.model.User
import mephi.isa.institute.studentaccountability.repository.IUserRepository
import mephi.isa.institute.studentaccountability.storage.AuthStorage
import mephi.isa.institute.studentaccountability.util.UnauthorizedException
import mephi.isa.institute.studentaccountability.util.DuplicateKeyException

class AdminInteractor(
    private val authStorage: AuthStorage,
    private val authRepository: IUserRepository
) {

    fun authenticate(login: String, password: String): String {
        val user = authRepository.authAdmin(login, password)
        return authStorage.putAdmin(user)
    }

    fun deauthenticate(token: String) {
        if (!authStorage.hasAdmin(token)) {
            throw UnauthorizedException("Token has expired")
        }
        authStorage.logoutAdmin(token)
    }

    fun listUsers(token: String): List<User> {
        // TODO rewrite to getAdmin!
        if (authStorage.hasAdmin(token)) {
            return authRepository.listUsers()
        }
        throw UnauthorizedException("Token has expired")
    }

    fun addUser(token: String, user: User, password: String) {
        if (authStorage.hasAdmin(token)) {
            if (user.name !in authRepository.listUsers().map{u -> u.name}) {
                authRepository.updateUser(user, password)
            } else {
                throw DuplicateKeyException("New user exists")
            }
            return
        }
        throw UnauthorizedException("Token has expired")
    }

    fun removeUser(token: String, user: User) {
        if (authStorage.hasAdmin(token)) {
            authRepository.removeUser(user)
            return
        }
        throw UnauthorizedException("Token has expired")
    }

    fun updateUser(token: String, user: User, password: String) {
        if (authStorage.hasAdmin(token)) {
            if (user.name in authRepository.listUsers().map{u -> u.name}) {
                authRepository.updateUser(user, password)
            } else {
                throw NotFoundException("User to update does not exists")
            }
            return
        }
        throw UnauthorizedException("Token has expired")
    }
}